-- Un client, SACQUET Bilbon, a commandé 
-- 4 places 
-- pour «La chute du faucon noir», 
-- le 27/10/2019 
-- à la salle «Le gouffre de Helm». 

-- Ecrivez les requêtes qui permettent de générer la facture correspondante. --


-- Créer Bilbon Sacquet --
INSERT INTO client(id, nom, prenom, civilite_id) VALUES ((SELECT NVL(MAX(id),0)+1 FROM Client), 'Sacquet', 'Bilbon', '2');
-- Créer la Salle --
INSERT INTO salle(id, nom, adresse, compladresse, codepostal, ville) VALUES ((SELECT NVL(MAX(id),0)+1 FROM salle), 'Le gouffre de Helm', '69 rue des boules de noël', 'Batiment 2 - étage 4', '98000', 'Paupolnor');
-- Créer les 10000 places --

Create PROCEDURE GENERATE_PLACES_SALLE 
(
	id_s salle.salle_id%TYPE, 
	NP sederoule.nbplacesdisponibles%TYPE
) AS
	rg INT;
    grd CHAR;
BEGIN
	rg := 0;
	FOR i in 0..NP LOOP
		IF i in (100,300,500,800) THEN 
            rg:=0 ;
        END IF;
		rg := rg + 1;
        
        CASE
            WHEN (i < 2) THEN grd:='A';
			WHEN (i > 2 AND i<4) THEN grd:='B';
			WHEN (i > 4 AND i<8) THEN grd:='C';
			WHEN (i >8 AND i<16) THEN grd:='D';
			WHEN (i >16 AND i<32) THEN grd:='E';
			WHEN (i >32 AND i<64) THEN grd:='F';
			WHEN (i >64 AND i<128) THEN grd:='G';
			WHEN (i >128 AND i<256) THEN grd:='H';
			WHEN (i >256 AND i<512) THEN grd:='I';
			WHEN (i >512 AND i<1024) THEN grd:='J';
			WHEN (i >1024 AND i<2048) THEN grd:='K';
			WHEN (i >2048 AND i<4096) THEN grd:='L';
			WHEN (i >4096 AND i<8192) THEN grd:='M';
			WHEN (i >8192 AND i<16384) THEN grd:='N';
			WHEN (i >16384 AND i<32768) THEN grd:='O';
			WHEN (i >32768 AND i<65536) THEN grd:='P';
			WHEN (i >65536 AND i<131072) THEN grd:='Q';
			WHEN (i >131072 AND i<262144) THEN grd:='R';
			WHEN (i >262144 AND i<524288) THEN grd:='S';
			WHEN (i >524288 AND i<1048576) THEN grd:='T';
			WHEN (i >1048576 AND i<2097152) THEN grd:='U';
			WHEN (i >2097152 AND i<4194304) THEN grd:='V';
			WHEN (i >4194304 AND i<8388608) THEN grd:='W';
			WHEN (i >8388608 AND i<16777216) THEN grd:='X';
			WHEN (i >16777216 AND i<33554432) THEN grd:='Y';
			ELSE grd:='Z';
        END CASE;
		
		INSERT INTO place (id, numero, Gradin, rangee, salle_id, categorie_prix_id)
		VALUES 	(	
					(SELECT NVL(MAX(id),0)+1 FROM place),
					i,
					grd,
					rg,
					id_s,					
					(select dbms_random.value(1,(SELECT count(id) from categorie_prix) num from dual)
				);
    END LOOP;
END;

-- Créer l'évènement --
INSERT INTO evenement (id, libelle, description) VALUES ((SELECT NVL(MAX(id),0)+1 FROM evenement), 'La chute du faucon noir', 'Un putain de faucon noir qui tombe de notre immeuble de 72 étage pour au final se fracasser la gueule au sol, humour, sensation et sang en effervéscence garantis!');
-- Affecter la Saller à l'évènement --
INSERT INTO sederoule (evenement_id, salle_id, nbplacesdisponibles, dateevenement) 
VALUES (
    (   
        SELECT e.id
        FROM EVENEMENT e
        WHERE e.libelle LIKE 'La chute du faucon noir'
    ),
    (   
        SELECT s.id
        FROM salle s
        WHERE s.nom LIKE 'Le gouffre de Helm'
    ), 
    10000, 
    '27/10/2019');
	
-- Création des billets non factueés --

BEGIN
	FOR i in 0..3 LOOP
		INSERT INTO billet (id, code, id_client, id_place, id_evenement)
		VALUES (
					(SELECT NVL(MAX(id),0)+1 FROM billet),
					'BIL-'||LPAD((SELECT NVL(MAX(id),0)+1 FROM billet), 27, '0'),------int en hexa------
					101,
					(
						SELECT MIN(place_salle.id_p)
						FROM (
								SELECT p.id as id_p  FROM PLACE p
								LEFT OUTER JOIN CATEGORIE_PRIX c on c.ID = p.CATEGORIE_PRIX_ID
								WHERE SALLE_ID = 101
							 ) place_salle
						WHERE place_salle.id_p NOT IN (
														SELECT p.id FROM BILLET b
														LEFT OUTER JOIN PLACE p on b.ID_PLACE = p.ID
														WHERE b.id_evenement = 101
													   )
					),
					101);	
	END LOOP;
END;

-------------------------------------------------


-- Mise en place de la facture --

-- Billet pour lévènement recherché pour le user d'id 101 Bilbon Sacquet --

SET SERVEROUTPUT ON

DECLARE 
    id_fact INT;
BEGIN
    SELECT NVL(MAX(id),0)+1 
    INTO id_fact
    FROM Facture;
    
    INSERT INTO FACTURE (ID,CLIENT_ID,REFERENCE, F_DATE)
    VALUES (
                id_fact,
                101,
                'FA'||TO_CHAR(LPAD(id_fact, 8, '0')),
                SYSDATE
            );
            
    UPDATE BILLET b
    SET b.id_facture = id_fact
    WHERE b.id_evenement IN (
                                SELECT EVENEMENT_ID
                                FROM SEDEROULE sdr
                                JOIN EVENEMENT e on e.ID = sdr.EVENEMENT_ID
                                JOIN SALLE s on s.ID = sdr.SALLE_ID
                                WHERE DATEEVENEMENT = '27/10/2019' 
                                AND e.LIBELLE LIKE 'La chute du faucon noir' 
                                AND s.NOM LIKE 'Le gouffre de Helm'
                             )
    AND b.ID_CLIENT = 101;
END;


