-- Donner la liste des événements et leur salle, triée par date décroissante pour les 
-- événements prévus entre aujourd’hui + 1 mois et aujourd’hui + 1 an et 1 mois.

SELECT e.libelle, s.nom, d.dateevenement as Salle from evenement e
LEFT OUTER JOIN sederoule d on d.evenement_id=e.ID
LEFT OUTER JOIN salle s on s.ID = d.salle_id
WHERE d.dateevenement > ADD_MONTHS(SYSDATE,1)
AND d.dateevenement < ADD_MONTHS(SYSDATE,13)
ORDER BY d.dateevenement DESC;
