-- Combien de clients n’ont pas de numéro de téléphone --
SET SERVEROUTPUT ON

DECLARE 
	nbphone INT;
BEGIN
	dbms_output.put_line('3 - Combien de clients n’ont pas de numéro de téléphone ? ');
	
    SELECT count(numTel)
    INTO nbphone
    FROM (
            SELECT count(p.phone_id) as numTel FROM client c 
            LEFT OUTER JOIN posseder p ON p.client_id=c.id
            GROUP BY nom, prenom
    )
	WHERE numTel = 0;
	dbms_output.put_line(nbphone||' utilisateur(s) ne possède(nt) pas de numéro de téléphone.');
END;