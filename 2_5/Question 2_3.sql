-- Ecrire une fonction qui permettra, 
--		- pour un événement donné, 
--		- un gradin 
--		- une rangée donnés 
--		- un nombre de places donné, 
-- de renvoyer la liste des identifiants des places disponibles contiguës (voisine).
-- Si il n’y a pas assez de places contiguës, la fonction renvoie le caractère zéro.
-- Si il y a plus de places contiguës que de places demandées sur la rangée, la fonction 
-- prendra les places par numéro croissant en partant du plus petit.
--
-- Exemple: 
-- Gradin B, Rangée 3: il reste 3 places contiguës dont les numéros sont 32, 33, 34.
--
-- La fonction est appelée avec une demande de 4 places. Il n’y en a pas assez elle 
-- renvoie donc «0».
--
-- La fonction est appelée avec une demande de 2 places, elle renvoie donc «32,33».


CREATE OR REPLACE FUNCTION get_Place_list (
											e_id 	evenement.id%TYPE;
											grd 	place.gradin%TYPE;
											rg		place.rangee%TYPE;
											nb_p	INT;
										)
RETURN INT
IS
	TYPE places_list IS VARRAY(nb_p) OF place.id%TYPE;  
    counter INT := 1;
    places_id places_list := places_list();
	PLACE_NOT_EXIST EXCEPTION;
BEGIN
	-- Récupère les ID des place non attribuée à des billets --
	 FOR i in (
				
                SELECT id
                FROM PLACE p
                LEFT OUTER JOIN SEDEROULE sdr ON p.SALLE_ID = sdr.SALLE_ID
                WHERE sdr.EVENEMENT_ID = e_id
                AND id NOT IN   (
                                                SELECT ID_PLACE
                                                FROM billet
                                                WHERE id_evenement = e_id
                                        )
                        AND p.RANGEE = rg
                        AND p.GRADIN = grd
            ) LOOP
        places_id.extend;
        places_id(COUNTER) := i.id; 
        COUNTER := COUNTER +1;
    END LOOP;
	
	
	IF places_id IS NULL  
	THEN
		RAISE PLACE_NOT_EXIST;
	END IF;
	
	-- Vérifier s'il existe un nombre de place consécutives libres --	
	for i in 1..nb_p LOOP
		
	END LOOP;

EXCEPTION
	WHEN PLACE_NOT_EXIST THEN dbms_output.put_line('Place not exist');
	
END;