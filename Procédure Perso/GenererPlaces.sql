Create or replace PROCEDURE GENERATE_PLACES_SALLE 
(
	id_s salle.id%TYPE, 
	NP sederoule.nbplacesdisponibles%TYPE
) AS
	rg INT;
    grd CHAR;
	
	cmpt NUMBER;
	nbr INT;
BEGIN
	rg := 0;
	cmpt := 1;
	nbr := 0;
	FOR i in 0..NP LOOP
	
		nbr := nbr + 1;
		
		IF nbr > 10 THEN
			rg := rg + 1;
			nbr := 0;
		END IF;
		
		IF (i/2 = cmpt AND i<33554432) THEN 
			cmpt := i;
            rg := 0;
			nbr := 0;
        END IF;	
        
        CASE
            WHEN (i < 2) THEN grd:='A';
			WHEN (i >= 2 AND i<4) THEN grd:='B';
			WHEN (i >= 4 AND i<8) THEN grd:='C';
			WHEN (i >=8 AND i<16) THEN grd:='D';
			WHEN (i >=16 AND i<32) THEN grd:='E';
			WHEN (i >=32 AND i<64) THEN grd:='F';
			WHEN (i >=64 AND i<128) THEN grd:='G';
			WHEN (i >=128 AND i<256) THEN grd:='H';
			WHEN (i >=256 AND i<512) THEN grd:='I';
			WHEN (i >=512 AND i<1024) THEN grd:='J';
			WHEN (i >=1024 AND i<2048) THEN grd:='K';
			WHEN (i >=2048 AND i<4096) THEN grd:='L';
			WHEN (i >=4096 AND i<8192) THEN grd:='M';
			WHEN (i >=8192 AND i<16384) THEN grd:='N';
			WHEN (i >=16384 AND i<32768) THEN grd:='O';
			WHEN (i >=32768 AND i<65536) THEN grd:='P';
			WHEN (i >=65536 AND i<131072) THEN grd:='Q';
			WHEN (i >=131072 AND i<262144) THEN grd:='R';
			WHEN (i >=262144 AND i<524288) THEN grd:='S';
			WHEN (i >=524288 AND i<1048576) THEN grd:='T';
			WHEN (i >=1048576 AND i<2097152) THEN grd:='U';
			WHEN (i >=2097152 AND i<4194304) THEN grd:='V';
			WHEN (i >=4194304 AND i<8388608) THEN grd:='W';
			WHEN (i >=8388608 AND i<16777216) THEN grd:='X';
			WHEN (i >=16777216 AND i<33554432) THEN grd:='Y';
			ELSE grd:='Z';
        END CASE;
		
		INSERT INTO place (id, numero, Gradin, rangee, salle_id, categorie_prix_id)
		VALUES 	(	
					(SELECT NVL(MAX(id), 0)+1 FROM place),
					nbr,
					grd,
					rg,
					id_s,					
					(select dbms_random.value(1,(SELECT count(id) from categorie_prix)) num from dual)
				);
    END LOOP;
END;