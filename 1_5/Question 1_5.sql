-- Donner, par clients, le nombre de billet commandés mais non facturés --

SELECT nom, prenom, nonPaye 
FROM client
JOIN (
            SELECT id_client, count(id_client) as nonPaye 
			FROM billet
            WHERE ID_FACTURE IS NULL
            GROUP BY ID_CLIENT
        ) r ON client.ID =r.ID_CLIENT
ORDER BY nom,prenom;
		
