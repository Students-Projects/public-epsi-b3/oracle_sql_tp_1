-- Lister le nom, prénom et civilité de tous les clients par ordre alphabétique. --

SELECT nom, prenom, libelle 
FROM client c 
JOIN civilite ON civilite.ID = c.civilite_id
ORDER BY nom, prenom;

