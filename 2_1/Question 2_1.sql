-- Ecrire une procédure qui prendra les paramètres nécessaires pour créer une facture à 
-- partir des billets commandés par un client. Cette procédure devra attendre en 
-- paramètres: 
-- 		- l’identifiant du client
-- 		- l’identifiant de la salle
-- 		- l’identifiant de l’événement
-- Elle devra traiter les erreurs si le client, la salle ou l’événement n’existent pas.


CREATE OR REPLACE PROCEDURE gen_facture (
											c_id client.id%TYPE,
											s_id salle.id%TYPE,
											e_id evenement.id%TYPE
										) 
IS
	id_fact facture.id%TYPE;
	E404_NOTFOUND EXCEPTION;
	NOT_EXIST EXCEPTION;
	
	
	ev_id_exist evenement.id%TYPE;
	ev_exist evenement.id%TYPE;
	s_id_exist salle.id%TYPE;
	c_id_exist client.id%TYPE;
	
	
BEGIN
	-- Si l'évènement choisi n'existe pas dans la salle -- 
	SELECT evenement_id INTO ev_exist
	FROM sederoule
	WHERE evenement_id = e_id
	AND salle_id = s_id;
	
	SELECT id INTO ev_id_exist
	FROM evenement
	WHERE id = e_id;
	
	SELECT id INTO s_id_exist
	FROM salle
	WHERE id = s_id; 
	
	SELECT id INTO c_id_exist
	FROM client
	WHERE id = c_id;
	
	
	IF ev_id_exist IS NULL 
	OR	ev_exist IS NULL
	OR	s_id_exist IS NULL
	THEN
		RAISE E404_NOTFOUND;
	END IF;
	
	IF c_id_exist IS NULL
	THEN
		RAISE NOT_EXIST;
	END IF;

	SELECT NVL(MAX(id),0)+1 INTO id_fact FROM Facture;
    
    INSERT INTO FACTURE (	ID,
							CLIENT_ID,
							REFERENCE, 
							F_DATE)
    VALUES (
                id_fact,
                c_id,
                'FA'||TO_CHAR(LPAD(id_fact, 8, '0')),
                SYSDATE
            );
            
    UPDATE BILLET b
    SET b.id_facture = id_fact
    WHERE b.id_evenement = e_id
    AND b.ID_CLIENT = c_id;
	
EXCEPTION
	WHEN E404_NOTFOUND THEN dbms_output.put_line('404 - Not Found');
	WHEN NOT_EXIST THEN dbms_output.put_line('Client Non existant');
	
END;
	