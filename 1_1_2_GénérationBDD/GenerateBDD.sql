-- 1 Créer la base de donnée --
	

-- Table des civilités (Mr, Mme, Melle, ...) --

CREATE TABLE civilite (
	id INT NOT NULL,
	libelle VARCHAR(50) NOT NULL,
	CONSTRAINTS PK_civilite PRIMARY KEY (id),
	CONSTRAINTS U_civilite UNIQUE (libelle)
);

-- Table Des numéros de téléphone --

CREATE TABLE telephone (
	id INT NOT NULL,
	numero VARCHAR(50) NOT NULL,
	CONSTRAINTS PK_tel PRIMARY KEY (id),
	CONSTRAINTS U_tel UNIQUE (numero)
);


-- Table des Catégories de prix --

CREATE TABLE categorie_prix (
	id INT NOT NULL,
	montant NUMBER (10, 2) NOT NULL,
	CONSTRAINTS PK_categorie_prix PRIMARY KEY (id)
);


-- Table Salle --

CREATE TABLE salle (
	id INT NOT NULL,
	nom VARCHAR(50) NOT NULL,
	adresse VARCHAR(250) NOT NULL,
	compladresse VARCHAR(250),
	codepostal CHAR(5) NOT NULL,
	ville VARCHAR(100) NOT NULL,
	CONSTRAINTS PK_salle PRIMARY KEY (id),
	CONSTRAINTS U_salle UNIQUE (nom,adresse,compladresse,ville,codepostal)
);

-- Table Client --

CREATE TABLE client (
	id INT NOT NULL,
	nom VARCHAR(50) NOT NULL,
	prenom VARCHAR(50) NOT NULL,
	civilite_id INT NOT NULL,
	CONSTRAINTS PK_Client PRIMARY KEY (id),
	CONSTRAINTS FK_Client_civilite FOREIGN KEY (civilite_id) REFERENCES civilite(id) ON DELETE CASCADE
);

-- Table de possession de numéro de téléphone --

CREATE TABLE posseder (
	client_id INT NOT NULL,
	phone_id INT NOT NULL,
	CONSTRAINTS FK_tel_posseder_client FOREIGN KEY (client_id) REFERENCES client(id) ON DELETE CASCADE,
	CONSTRAINTS FK_client_posseder_tel FOREIGN KEY (phone_id) REFERENCES telephone(id) ON DELETE CASCADE,
	CONSTRAINTS PK_posseder PRIMARY KEY (client_id, phone_id)
);

-- Table des Factures --

CREATE TABLE facture (
	id INT NOT NULL,
	f_date DATE NOT NULL,
	reference VARCHAR(10) NOT NULL,
	client_id INT NOT NULL,
	CONSTRAINTS PK_facture PRIMARY KEY (id),
	CONSTRAINTS FK_facture_client FOREIGN KEY (client_id) REFERENCES client(id) ON DELETE CASCADE,
	CONSTRAINTS U_ref_facture UNIQUE (reference)
);


-- Table des Places -- 

CREATE TABLE place (
	id INT NOT NULL,
	numero INT NOT NULL,
	gradin CHAR NOT NULL,
	rangee INT NOT NULL,
	salle_id INT NOT NULL,
	categorie_prix_id INT NOT NULL,
	CONSTRAINTS PK_place PRIMARY KEY (id),
	CONSTRAINTS FK_salle_place FOREIGN KEY (salle_id) REFERENCES salle(id) ON DELETE CASCADE,
	CONSTRAINTS FK_prix_place FOREIGN KEY (categorie_prix_id) REFERENCES categorie_prix(id) ON DELETE CASCADE,
	CONSTRAINTS U_place UNIQUE (salle_id, gradin, rangee, numero)
);

-- Table des évènements --

CREATE TABLE evenement (
	id INT NOT NULL,
	libelle VARCHAR(50) NOT NULL,
	description VARCHAR2(4000),
	CONSTRAINTS PK_ev PRIMARY KEY (id)
);

-- Table de liaison salle-evenement --

-- La salle ne pouvant pas être empruntée deux fois sur la même date on set salle_id allié à la dateevenement en tant que valeur unique.

CREATE TABLE sederoule(
	evenement_id INT NOT NULL,
	salle_id INT NOT NULL,
	nbplacesdisponibles INT NOT NULL,
	dateevenement DATE NOT NULL,
	CONSTRAINTS PK_sederoule PRIMARY KEY (evenement_id, salle_id, dateevenement),
	CONSTRAINTS FK_salle_pour_evenement FOREIGN KEY (salle_id) REFERENCES salle(id),
	CONSTRAINTS FK_evenement_dans_salle FOREIGN KEY (evenement_id) REFERENCES evenement(id),
	CONSTRAINTS U_date_salle UNIQUE (salle_id, dateevenement)
);


-- Table des Billets --

CREATE TABLE billet(
	id INT NOT NULL,
	code VARCHAR(250) UNIQUE NOT NULL,
	id_facture INT,
	id_client INT NOT NULL,
	id_place INT NOT NULL,
	id_evenement INT NOT NULL,
	CONSTRAINTS PK_billet PRIMARY KEY (id),
	CONSTRAINTS U_place_ev UNIQUE (id_place, id_evenement),
	CONSTRAINTS U_facture_billet UNIQUE (code, id_facture),
	CONSTRAINTS FK_billet_facture FOREIGN KEY (id_facture) REFERENCES facture(id),
	CONSTRAINTS FK_billet_client FOREIGN KEY (id_client) REFERENCES client(id),
	CONSTRAINTS FK_billet_place FOREIGN KEY (id_place) REFERENCES place(id),
	CONSTRAINTS FK_billet_ev FOREIGN KEY (id_evenement) REFERENCES evenement(id)	
);
