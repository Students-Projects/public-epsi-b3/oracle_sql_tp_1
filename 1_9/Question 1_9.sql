-- Ecrivez les requêtes qui permettront à Gimli de commander 8 places pour 
-- accompagner Bilbon au même événement et d’obtenir sa facture dans la foulée. --


SET SERVEROUTPUT ON

-- Créer Bilbon Sacquet --
INSERT INTO client(id, nom, prenom, civilite_id) VALUES ((SELECT MAX(id)+1 FROM Client), 'deméboule', 'Gimli', '2');

-- Commande de Billet --

DECLARE 

	gimly_id INT;
	event_id INT;
	id_fact INT;
	id_max_billet INT;

BEGIN
	-- Vérifier si les places près de Bilbon sont libre --
	-- Sinon prendre 8 places consécutive --
	-- Sinon prendre juste 8 places -- 
	gimly_id := 0;
	event_id := 0;
	id_fact := 0;
	
	
	SELECT id INTO gimly_id
	FROM Client
	WHERE LOWER(prenom) LIKE 'gimli'
	OR LOWER (nom) LIKE 'gimli';
	
	SELECT MAX(b.id) , e.id INTO id_max_billet,event_id
	FROM client c
	LEFT OUTER JOIN billet b ON b.ID_CLIENT = c.ID
	LEFT OUTER JOIN EVENEMENT e ON b.ID_EVENEMENT = e.id
	WHERE LOWER(c.prenom) LIKE 'bilbon' AND LOWER(c.nom) Like 'sacquet'
	GROUP BY e.id;
	
    FOR i IN 0..7 LOOP
        INSERT INTO billet (id, code, id_client, id_place, id_evenement)
		VALUES (
					(SELECT NVL(MAX(id),0)+1 FROM billet),
					'BIL-'||LPAD(i, 27, '0'),------int en hexa------
					gimly_id,
					(
						SELECT MIN(place_salle.id_p)
						FROM (
								SELECT p.id as id_p  FROM PLACE p
								LEFT OUTER JOIN CATEGORIE_PRIX c on c.ID = p.CATEGORIE_PRIX_ID
								WHERE SALLE_ID = 101
							 ) place_salle
						WHERE place_salle.id_p NOT IN (
														SELECT p.id FROM BILLET b
														LEFT OUTER JOIN PLACE p on b.ID_PLACE = p.ID
														WHERE b.id_evenement = 101
													   )
					),
					event_id
				);
    END LOOP;
	
	-- Création de la facture --
	
	SELECT NVL(MAX(id),0)+1 INTO id_fact FROM Facture;
    
    INSERT INTO FACTURE (ID,CLIENT_ID,REFERENCE, F_DATE)
    VALUES (
                id_fact,
                gimly_id,
                'FA'||TO_CHAR(LPAD(id_fact, 8, '0')),
                SYSDATE
            );
            
    UPDATE BILLET b
    SET b.id_facture = id_fact
    WHERE b.id_evenement IN (
                                SELECT EVENEMENT_ID
                                FROM SEDEROULE sdr
                                JOIN EVENEMENT e on e.ID = sdr.EVENEMENT_ID
                                JOIN SALLE s on s.ID = sdr.SALLE_ID
                                WHERE DATEEVENEMENT = '27/10/2019' 
                                AND e.LIBELLE LIKE 'La chute du faucon noir' 
                                AND s.NOM LIKE 'Le gouffre de Helm'
                             )
    AND b.ID_CLIENT = gimly_id;
	
	
END;