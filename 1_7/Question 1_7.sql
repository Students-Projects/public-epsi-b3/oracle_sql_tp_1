-- Donner par clients et par factures le nombre de places achetées --

SELECT  c.nom||' '||c.prenom as "Client", 
        NVL(TO_CHAR(F.id), 'Sans Facture') as "Facture", 
        NVL(TO_CHAR(B.nb_billet), 'Aucun Billet Facturé') as "Places Acheté", 
        NVL(TO_CHAR(R.nb_billet), 'Aucun Billet Réservé') as "Places Non Facturée"
FROM client C
LEFT OUTER JOIN (
                    SELECT f.id, c.id as identifiant
                    FROM facture f
                    LEFT OUTER JOIN client c ON f.client_id = c.id
                ) F ON C.id = F.identifiant
LEFT OUTER JOIN (
                    SELECT f.id, count(b.id) as nb_billet
                    FROM facture f
                    LEFT OUTER JOIN billet b ON b.id_facture = f.id
                    GROUP BY f.id   
                ) B ON F.id = B.id
LEFT OUTER JOIN (
                    SELECT c.id, count(b.id) as nb_billet
                    FROM CLIENT c
                    LEFT OUTER JOIN billet b ON b.id_client = c.id
                    GROUP BY c.id   
                ) R ON c.id = R.id
ORDER BY "Client", "Places Acheté";