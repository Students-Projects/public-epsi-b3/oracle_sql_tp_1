-- Donner le nombre de numéros de téléphones par client classés par nombre 
-- décroissant et par nom et prénom croissant.

SELECT nom, prenom, count(p.phone_id) AS Telephones 
FROM client c 
LEFT OUTER JOIN posseder p ON p.client_id=c.ID
GROUP BY nom, prenom
ORDER BY 	Telephones DESC,
			nom, prenom ASC
;
