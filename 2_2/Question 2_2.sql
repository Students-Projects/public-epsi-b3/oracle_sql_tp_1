-- Ecrire une procédure qui permettra d’enregistrer une réservation pour 1 ou plusieurs 
-- places pour un événement pour un client donné.
-- La procédure prendra en paramètres 
--		- l’identifiant du client 
--		- l’identifiant de l’événement, 
-- 		- le nombre de places commandées.

CREATE OR REPLACE PROCEDURE reserve (
										c_id client.id%TYPE,
										e_id evenement.id%TYPE,
										nb INT
									) 
IS
	new_id billet.id%TYPE;
BEGIN
	
	FOR i in 0..nb-1 LOOP
		
		SELECT NVL(MAX(id),0)+1 INTO new_id
		FROM billet; 
	
		INSERT INTO billet (id, code, id_client, id_place, id_evenement)
		VALUES (
					new_id,
					'BIL-'||LPAD(new_id, 27, '0'),------int en hexa------
					c_id,
					(
						SELECT MIN(p.id) 
						FROM PLACE p
						WHERE p.id NOT IN 	(
												SELECT p.id FROM BILLET b
												LEFT OUTER JOIN PLACE p on b.ID_PLACE = p.ID
												WHERE b.id_evenement = e_id
											)
					),
					c_id);	
	END LOOP;
	
END;