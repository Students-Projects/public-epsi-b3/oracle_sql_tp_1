-- Création d'un nouveau User et de sa BDD puis lui ajouter ses droits --

CREATE USER julien_lauret IDENTIFIED BY code;
GRANT CONNECT, RESOURCE, DBA TO julien_lauret;